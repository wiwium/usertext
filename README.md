## UserText

### Info
1) Usertext module is use to find user visible text from given html pages using multiprocessing.
2) It also parse the phone numbers from usertext 

### Usecase

1) Install packages from requirements.txt
   ```bash
   pip3 install -r requirements.txt
   ```
2) Just copy the usertext folder into your project.
3) Then import in following format
   ```python
   from usertext import ut
   ```
4) implement UserText class thats it.. It takes following Parameters 
( note : everything defaults from config.py file)
   
----------

* *zip_path* : str

  * html zip file path Containing downloaded htmls
  * each .html in its own folder 
        And that folders name is taken as 'id'
* *ip* : str
  * ip to connect to mongodb
* *port* : int
  * mogodb port
* *db_name* : str
  * database name
* *db_ut_name* : str
  * collection name in which usertext is to be stored
* *db_tel_name*: str
  * collection name in which tel are to be stored
* *is_tel* : boolean
  * default True
  * if false phone numbers will not be parsed
* *cap* : int
  * default None
  * For testing on small data-set

5) Also instead of using arguments use config.py file  
by updating default values there then use

   ```python
   ut.UserText()
   ```

6) Methods
   ```python
   ut.join_tel(href_tel,ut_tel)
   ```
   returns <pd.DataFrame()>
   use to join two pandas DataFrame of telephone got form href and form usertext
