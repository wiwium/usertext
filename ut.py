try:
    import config
except:
    from usertext import config
import time
import zipfile
from multiprocessing.dummy import Pool  # This is a thread-based Pool
from multiprocessing import cpu_count

import pymongo
from bs4 import BeautifulSoup
from bs4.element import Comment
import re
import pandas as pd

# regex = re.compile(r'[0-9!@#$%\^&\*\(\)\._=\-\\|\{\}\[\]\\:"\'+;<>,?/~`]')

'''
----------
    zip_path : str, html zip file path 
        Containing downloaded htmls such that each .html in its own folder 
        And that folders name is taken as 'id'
        
    ip : str, ip to connect to mongodb
    port : int, mogodb port
    
    db_name : str, database name
    db_ut_name : str, collection name in which usertext is to be stored
    db_tel_name: str, collection name in which tel are to be stored
    
    is_tel : boolean, default True
        If False phone numbers will not be parsed
    cap : int, default None
        For testing on small data-set

'''
class UserText:
    
    def __init__(self,zip_path=config.PATHS['ZIP'],\
                ip=config.PATHS['Mongo_ip'],port=config.PATHS['Mongo_port'],\
                db_name=config.PATHS['mongo_database'],db_ut_name=config.PATHS["out_db"]['usertext'],\
                db_tel_name=config.PATHS["out_db"]['tel'],\
                is_tel=True,cap=None):
        
        self.is_tel = is_tel
        
        connection = pymongo.MongoClient(ip,port)
        db = connection[db_name]

        
        self.html_zip = zipfile.ZipFile(zip_path)
        self.html_list = [d for d in self.html_zip.namelist() if d.endswith(".html")]
        self.db_usertext = db[db_ut_name]
        self.db_tel = db[db_tel_name]
        self.num_re = re.compile(r'[^0-9][ ][+]?([ ]?[\(\)\-]?[ ]?[0-9]){10,12}[ ][^0-9]')


        start = time.time()
        print('total documents ',len(self.html_list))
        pool = Pool(cpu_count() * 4)
        pool.map(self.read_file, self.html_list[:cap])
        print(time.time()-start)
        
        # drop duplicates from phone numbers
        db.eval('''
    db.%s.aggregate([ { $match: {}},{ $group: { 
        _id: { tel: "$tel"}, 
        dups: { "$addToSet": "$_id" }, 
        count: { "$sum": 1 } 
      }}, { $match: { 
        count: { "$gt": 1 }}}],
    {allowDiskUse: true})
    .forEach(function(doc) {
        db.%s.remove({_id : {$in: doc.dups }});
    })
    '''%(db_tel_name,db_tel_name))

        

    def read_file(self,filename):

        company_id = filename.split(r'/')[-2]
        try:
            with self.html_zip.open(filename) as html_file:
                content = html_file.read()
            text = UserText.bs(content)
            self.db_usertext.insert({'company_id': company_id, 'visible_text': text})
            if self.is_tel:
                self.get_tel(company_id,text)
        except:
            print('error in one document')
            return
        

    def get_tel(self,company_id,text):
        tel = [i.group()[1:-1].strip() for i in self.num_re.finditer(text)]
        if len(tel) <= 0:
            return
        df = pd.DataFrame({'id' : company_id,'tel':tel})
        
        df = df[~df['tel'].apply(lambda x: re.sub(r'[\D]','',str(x))).duplicated(keep='first')]
        
        self.db_tel.insert_many(df.to_dict('records'))
    

    @staticmethod
    def tag_visible(element):
        if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
            return False
        if isinstance(element, Comment):
            return False
        return True

    @staticmethod
    def bs(content):
        soup = BeautifulSoup(content.decode('utf-8', 'ignore'), 'html.parser')
        texts = soup.findAll(text=True)
        visible_texts = filter(UserText.tag_visible, texts)  
        return u" ".join(t.strip() for t in visible_texts)
    

# json :- mongoexport --db <db name> --collection <collection name> --out "<filepath.json>"
# csv :- mongoexport --db <db name> --collection <collection name> --type=csv --fields <field1>,<fileld2> --out "<filepath.csv>"

def join_tel(href_tel,ut_tel):
    
    href_tel.rename({'company_id':'id','link':'tel'},axis=1,inplace=True)
    href_tel['tel'] = href_tel['tel'].apply(lambda x:re.sub(r'[^+\. \-\(\)0-9]','',str(x)))
    df = ut_tel.append(href_tel)
    df = df[~df['tel'].apply(lambda x: re.sub(r'[\D]','',str(x))).duplicated(keep='first')]
    return df
    
    
if __name__ == "__main__":
    #testing
    UserText()